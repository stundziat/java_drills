public abstract class AbstractPersonClass
{
    // ***** Member Variables *****

    private String name_;
    private char gender_;
    private int age_;

    // ***** Constructors *****

    public AbstractPersonClass() {}

    public AbstractPersonClass(String new_name, char new_gender, int new_age)
    {
        name_ = new_name;
        gender_ = new_gender;
        age_ = new_age;
    }

    // ***** Functions *****

    public abstract void printPerson();
}
