import java.util.*;


// sout and then tab can be used to auto complete

// ********** JAVA FUNCTIONS **********

public class Main
{

    public static void example()
    {
        System.out.println("Call a functions");
    }

    // 1) Create function that checks for odd

    public static boolean isOdd(int num1)
    {
        int is_odd = num1 % 2;
        return (is_odd != 0);
    }

    // 2) Math function

    public static int add(int a, int b) { return a + b; }
    public static int subtract(int a, int b) { return a - b; }
    public static int multiply(int a, int b) { return a * b; }
    public static int divide(int a, int b) { return a / b; }
    public static int modulus(int a, int b) { return a % b; }

    // 3) Hello World

    public static void helloWorld(String your_name)
    {
        System.out.println("Helloworld to " + your_name);
    }

    // 4) Test for positive number

    public static boolean isPositive(int a) { return (a >= 0); }

    // 5) Test for equality of strings

    public static boolean isMatchStrings(String str1, String str2) { return (str1.equals(str2)); }


    // ********** JAVA OBJECTS **********

    // 1) Find the higher of two numbers

    public static double largerNumber(double num1, double num2)
    {
        return Math.max(num1, num2);
    }

    // 2) Find absolute value of a double value

    public static double absoluteValueOfNumber(double number)
    {
        return Math.abs(number);
    }

    // 3) Create a random number generator

    public static int randomNumber(int n, int m)
    {
        Random rand = new Random();
        int in_bounds = -1;

        while ( in_bounds < n || in_bounds > m )
        {
            in_bounds = rand.nextInt();
        }

        return in_bounds;
    }


    // ********** JAVA LOOPS **********

    public static void studentNameAndGrade(Student[] studs)
    {
        System.out.println("\n");
        for (int i = 0; i < studs.length; i++)
        {
            System.out.println("Student #" + i + " " + studs[i].name() + " " + studs[i].grade());
        }
    }

    public static void studentNameAndGPA(Student[] studs)
    {
        System.out.println("\n");
        for (int i = 0; i < studs.length; i++)
        {
            System.out.println("Student #" + i + " " + studs[i].name() + " " + studs[i].gpa());
        }
    }

    public static void breakThings()
    {
        System.out.println("Start");
        double num = 0.0;
        double max = 2147483648.0;
        while ( num <=  max)
        {
            num += 2;
        }
        System.out.println("End");
    }

    public static void breakThingsAgain()
    {
        double count = 0.0;
        double delta = 2.0;
        double max = 2147483648.0;
        do
        {
            count += delta;
        } while ( count <= max );
    }

    public static int findPrimeNumber(int number)
    {
        // WRONG
        while ( (number % 2 == 1) && (number % 3 != 0) )
        {
            number++;
        }

        return number;
    }


    // ********** JAVA DRILLS **********

    // Next largest prime number; recall prime number is divisible by only 1 and itself
    public static int nextPrime(int num)
    {
        int prime = 0;
        int factors = 0;
        boolean is_prime = false;

        while ( !is_prime )
        {
            num += 1;

            for (int i = 1; i <= num / 2; i++)
            {
                if (num % i == 0)
                {
                    factors++;
                }

                if (factors > 1)
                {
                    break;
                }
            }

            if (factors ==1)
            {
                is_prime = true;
                prime = num;
            }

            factors = 0;
        }

        return prime;
    }

    // Rate scrabble word score
    public static String scrabbleScorer(String[] candidates)
    {
        String[] word_array;
        int highest_value = 0;
        int value = 0;
        String highest_word = "";

        // for each loop: "For every word that is of type String within candidates, do..."
        for (String word: candidates)
        {
            word_array = word.split("");
            value = scoreTable(word_array);

            if (value > highest_value)
            {
                highest_value = value;
                highest_word = word;
            }
        }

        return highest_word;
    }

    // Assistance method for scrabble
    public static int scoreTable(String[] letters)
    {
        int score = 0;
        for (String letter: letters)
        {
            switch (letter)
            {
                case "A":
                case "a":
                case "E":
                case "e":
                case "I":
                case "i":
                case "O":
                case "o":
                case "U":
                case "u":
                case "L":
                case "l":
                case "N":
                case "n":
                case "S":
                case "s":
                case "T":
                case "t":
                case "R":
                case "r":
                    score += 1;
                    break;
                case "D":
                case "d":
                case "G":
                case "g":
                    score += 2;
                    break;
                case "B":
                case "b":
                case "C":
                case "c":
                case "M":
                case "m":
                case "P":
                case "p":
                    score += 3;
                    break;
                case "F":
                case "f":
                case "H":
                case "h":
                case "V":
                case "v":
                case "W":
                case "w":
                case "Y":
                case "y":
                    score += 4;
                    break;
                case "K":
                case "k":
                    score += 5;
                    break;
                case "J":
                case "j":
                case "X":
                case "x":
                    score += 8;
                    break;
                case "Q":
                case "q":
                case "Z":
                case "z":
                    score += 10;
                    break;
            }

        }

        return score;

    }

    // Find the fibonacci sequence based on a number of steps you want to walk
    public static int[] fibbonacci(int steps)
    {
        int[] fib_sequence = new int [steps];
        if (steps >= 2)
        {
            fib_sequence[0] = 0;
            fib_sequence[1] = 1;
        }
        else if (steps == 1)
        {
            fib_sequence[0] = 0;
        }

        for (int i = 2; i < fib_sequence.length; i++)
        {
            fib_sequence[i] = fib_sequence[i - 1] + fib_sequence[i - 2];
        }

        return fib_sequence;
    }

    // Most frequent Character - TODO: FINISH THIS
    public static char mostFrequentChar(char[] list)
    {
        char most_char = 'a';
        HashMap<Character, Integer> count_tracker = new HashMap<>();
        int value;

        for (char letter: list)
        {
            if(!count_tracker.containsKey(letter))
            {
                count_tracker.put(letter, 1);
            }
            else
            {
                value = count_tracker.get(letter) + 1;
                count_tracker.replace(letter, value);
            }
        }

        Set keys = count_tracker.keySet();
        Collection list_two = (Collection) count_tracker;

        return most_char;
    }

    // Sort a string
    public static String sortString(String str)
    {
        char[] temp_array = str.toCharArray();
        Arrays.sort(temp_array);
        return String.copyValueOf(temp_array);
    }

    // Reverse a string
    public static String reversal(String str)
    {
        String reverse="";
        for (int i = str.length()-1; i >= 0; i--)
        {
            reverse += str.charAt(i);
        }

        return reverse;
    }

    // Replace vowel pieces
    public static String vowelReplacer(String str)
    {
        String builder = "";

        for (int i = 0; i < str.length(); i++)
        {
            if (str.charAt(i) == 'a' || str.charAt(i) == 'e' || str.charAt(i) == 'i' || str.charAt(i) == 'o' || str.charAt(i) == 'u')
            {
                builder += vowelDictionary(str.charAt(i));
            }
            else
            {
                builder += str.charAt(i);
            }
        }

        return builder;
    }

    // Vowel Dictionary
    public static int vowelDictionary(char letter)
    {
        switch ( letter )
        {
            case 'a':
                return 1;
            case 'e':
                return 2;
            case 'i':
                return 3;
            case 'o':
                return 4;
            case 'u':
                return 5;
            default:
                return 20;
        }
    }

    // Check for vowels with a square
    public static int[] vowelSquare(char[][] square)
    {
        int[] idex = {0, 0};

        for (int i = 0; i < square.length; i++)
        {
            for (int j = 0; j < square[i].length; j++)
            {
                // Exceptions. These can be handled in the code
                try
                {
                    if (isVowel(square[i][j]) && isVowel(square[i][j + 1]) && isVowel(square[i + 1][j]) && isVowel(square[i + 1][j + 1]))
                    {
                        idex[0] = i;
                        idex[1] = j;
                    } else
                    {
                        idex[0] = -1;
                        idex[1] = -1;
                    }
                } catch ( ArrayIndexOutOfBoundsException e)
                {
                    continue;
                }
            }
        }

        return idex;
    }

    // Check if a vowel
    public static boolean isVowel(char letter)
    {
        switch (letter)
        {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                return true;
            default:
                return false;
        }
    }

    // Main function for week 2 Java work
//    public static void main(String[] args)
//    {
////        // ***** Java Functions *****
////        example();
////        System.out.println("Is odd: " + isOdd(10));
////
////        // ***** Java Objects *****
////        System.out.println("Larger of 2 numbers: " + largerNumber(150.0, 174.0));
////        System.out.println("Absolute value of number: " + absoluteValueOfNumber(-217.5));
////        System.out.println("In bounds number: " + randomNumber(10, 100));
////
////        Student tom = new Student("Tom", 'M', 4.0, 18, 27);
////        Student colby = new Student("Colby", 'M', 3.0, 14, 24);
////        tom.showStudent();
////
////        // Arrays
////        Student[] studs = new Student[2];
////        studs[0] = tom;
////        studs[1] = colby;
////        System.out.println("Student GPA's [Array]: " + studs[0].gpa() + " " + studs[1].gpa());
////
////        ArrayList<Student> studs_dynamic = new ArrayList<>(2);
////        studs_dynamic.add(tom);
////        studs_dynamic.add(colby);
////        System.out.println("Student GPA's [Arraylist]: " + studs_dynamic.get(0).gpa()
////                + " " + studs_dynamic.get(1).gpa());
////
////        // 1) Loop through Student array and list name/grade
////        studentNameAndGrade(studs);
////
////        // 2) Loop through Students array and list name/gpa
////        studentNameAndGPA(studs);
////
////        // 3) Loop through and make a large number
////        breakThings();
////
////        // 4) Loop through and make a large number again
////        breakThingsAgain();
//
//
//
//        // ********** JAVA DRILLS **********
//
//        // 1) Find next prime number from some position
//        // 1) Prime numbers
//        System.out.println("Prime Number: " + nextPrime(5));
//
//        // 2) Play scrabble
//        String[] words = {"cow", "zebra", "dog"};
//        System.out.println("Highest word: " + scrabbleScorer(words));
//
//        // 3) Find fibonacci sequence
//        int steps = 7;
//        int[] sequence = fibbonacci(steps);
//        for (int i = 0; i < steps; i++) { System.out.println("Fib Sequence: " + sequence[i]); }
//
//        // 4)
//
//        // 7) Check for vowels inside of a matrix
//        char[][] char_square = {{'c', 'u', 'v'}, {'a', 'e', 'u'}, {'x', 'y', 'z'}};
//        System.out.println("Returning index of vowel section: " + vowelSquare(char_square)[0] + ", " + vowelSquare(char_square)[1]);
//
//        // 9) Reverse a string
//        System.out.println("Tom in reverse is fun: " + reversal("Tom"));
//
//        // 10) Mess with some vowels
//        System.out.println("Vowel stuff: " + vowelReplacer("avocado"));
//
//    }


    public static void main(String[] args)
    {
        // ***** Abstract Classes *****

        // 1) Create circle class with some functions
        CircleClass circle = new CircleClass(5.0);
        circle.printCircle();

        // 2) Create abstract shape class with some functions
        // - See the AbstractShapeClass.java file for this

        // 3) Extend the shape class onto the circle class to find area
        // - See the the CircleClass.java file for this

        // 4) Create abstract person class
        // - See AbstractPersonClass.java

        // 5) Extend the Student class so that it inherits from PersonClass
        // - See the Student.java file.

        // ***** Interfaces *****

        // 1) Create a rectangle interface with two parameters and two functions
        // - See RectangleInterface.java file.

        // 2) Create a square class. Override functions from rectangle interface.
        SquareClass square = new SquareClass(5);
        square.printSquare();
    }

}
