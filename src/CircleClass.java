public class CircleClass extends AbstractShapeClass
{
    // ***** Member variables
    private double radius_;


    // ***** Constructors
    public CircleClass()
    {
        super();
        System.out.println("Hi Circle class!");
    }

    public CircleClass(double radius)
    {
        super(radius);
        radius_ = radius;
    }

    // ***** Set & Get
    public double radius() { return radius_; }
    public void setRadius(double new_radius) { radius_ = new_radius; }

    // ***** Functions

    // Find the circumference of this circle
    public double findCircumference()
    {
        return 2 * Math.PI * radius_;
    }

    // YOU MUST IMPLEMENT THE FUNCTIONS FROM THE ABSTRACT CLASS
    public double findPerimeter()
    {
        return findCircumference();
    }

    // Find the area of this circle
    public double findArea()
    {
        return Math.PI * radius_ * radius_;
    }

    // Print the circumference and radius
    public void printCircle()
    {
        String info = "Circumference: " + findCircumference() + " | Area: " + findArea();
        System.out.println(info);
    }
}
