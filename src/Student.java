public class Student extends AbstractPersonClass
{
    // Member Variables
    private String name_;
    private char gender_;
    private double gpa_;
    private int grade_;
    private int age_;

    // Constructors *****
    public Student(String name, char gender, double gpa, int grade, int age)
    {
        super(name, gender, age);

        name_ = name;
        gender_ = gender;
        gpa_ = gpa;
        grade_ = grade;
        age_ = age;
    }

    // Getter *****
    public String name() { return name_; }
    public char gender() { return gender_; }
    public double gpa() { return gpa_; }
    public int grade() { return grade_; }
    public int age() { return age_; }

    // Setter *****
    public void setName(String new_name) { name_ = new_name; }
    public void setGender(char new_gender) { gender_ = new_gender; }
    public void setGPA(double new_gpa) { gpa_ = new_gpa; }
    public void setGrade(int new_grade) { grade_ = new_grade; }
    public void setAge(int new_age) { age_ = new_age; }

    // Functions *****
    public void showStudent()
    {
        System.out.println("Name: " + name_ + "| Gender: " + gender_ + "| GPA: " + gpa_ +
                            "| Grade: " + grade_ + "| Age: " + age_);
    }

    @Override
    public void printPerson()
    {
        showStudent();
    }
}



