public class SquareClass implements RectangleInterface
{
    // ***** Member Variables *****

    private double side_length_;

    // ***** Constructors

    public SquareClass(double new_side_length)
    {
        side_length_ = new_side_length;
    }

    // ***** Functions *****

    public double calculatePerimeter()
    {
        return side_length_ * 4;
    }

    public double calculateArea()
    {
        return side_length_ * side_length_;
    }

    public void printSquare()
    {
        System.out.println("Perimeter: " + calculatePerimeter() + " | Area: " + calculateArea());
    }
}
