public interface RectangleInterface
{
    static final double length_ = 10.0;
    static final double width_ = 5.0;

    public double calculatePerimeter();
    public double calculateArea();
}
