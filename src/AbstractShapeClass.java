public abstract class AbstractShapeClass
{
    // ***** Member Variables *****

    private double length_;

    // ***** Constructors *****

    public AbstractShapeClass(){}

    public AbstractShapeClass(double new_length)
    {
        length_ = new_length;
    }

    // ***** Functions *****

    // Find the perimeter of the shape
    public abstract double findPerimeter();

    // Find the area of the shape
    public abstract double findArea();
}
